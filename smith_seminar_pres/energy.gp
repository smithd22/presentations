set term post enh color
set output "energy.ps"

set ylabel "Energies"
set xlabel "J_z / J_x"
set style line 1 lc 1 lw 2
set style line 2 lc 3 lw 2
unset key
file='gsenergy_DsppN16Ns8Jx1.00.dat'
p file u 3:4 w l ls 1 t "Energy Levels"

set output "gap.ps"
set ylabel "Gap"
set yrange[0:*]
p file u 3:6 w l ls 1 t "Gap"

set output "energy+gap.ps"
set ylabel "Energies"
set y2label "Gap"
set y2tics
set key top center
set ytics nomirror
set yrange[*:*]
set y2range[0:*]
p file u 3:4 w l ls 1 t "Energy Levels",\
  file u 3:6 w l ls 2 axes x1y2 t "Gap"
